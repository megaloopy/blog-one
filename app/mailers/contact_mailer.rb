class ContactMailer < ActionMailer::Base
  default to: "pixelpedro@gmail.com"
  
  def contact_email(name, email, message)
    @name = name
    @email = email
    @message = message
    
    mail(from: email, subject: 'BlogOne message from cotact form')
  end
end