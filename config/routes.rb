Rails.application.routes.draw do
  
  devise_for :users, :controllers => { registrations: 'registrations' }
  resources :articles do
    resources :comments
  end
  resources :contacts
  root to: 'pages#index'
  get 'pages/about'
  #get 'name', 'pages#about' --> to assign a name to the path
  get 'pages/contact'

end
 